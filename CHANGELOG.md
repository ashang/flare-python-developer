# Changelog

## [4.0] - 2020-05-02
- No changes in this version

## [3.0] - 2020-01-30
### Changed
- Updates to support the Flare 3.0 Python API

### Removed
- The Python Notebook extension has been moved to the flare-python-extensions/featured repository

## [2.0] - 2018-07-25
### Added
- Example on how to create a GUI using QtDesigner (qtdesignerexample)
- Example on how to add controls to the ribbon (ribbon)
- Example on how to add context menu items (contextmenus.py)
- Example on how to download and add proteins (download1oit.py)
- Example on how to update ligand properties using a rest service (restexample.py, runrestservice.py)

## [2.0] - 2018-11-28
### Added
- Python Notebook extension (pythonnotebook)
