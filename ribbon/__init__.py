# -*- coding: utf-8 -*-
# Copyright (C) 2020 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Demonstrates how to create a ribbon tab and group containing various buttons and widgets."""
import os

from cresset import flare
from PySide2 import QtCore, QtWidgets


@flare.extension
class RibbonTabExtension:
    """Create a ribbon group containing buttons and widgets."""

    def __init__(self):
        self._slider = None
        self._spin_box = None

    def load(self):
        """Load the extension."""
        main_window = flare.main_window()
        # Create the Developer tab if it does not exist
        ribbon_tab = main_window.ribbon["Developer"]
        # A tip key is used as a shortcut to switch between ribbon tabs
        # To show the TipKeys press and release the ALT key.
        # Over the "Developer" tab "D" will be shown. Pressing "D"
        # will switch to the tab
        ribbon_tab.tip_key = "D"

        # Create the Ribbon Example group if it does not exist
        group = ribbon_tab["Ribbon Example"]

        # Create a large button called "Large Button".
        # When pressed the self._show_dialog method will be called.
        control = group.add_button("Large Button", self._show_dialog)
        # Display the "flare_logo.png" image as the button icon
        # instead of the default icon.
        control.load_icon(self._plugin_file("flare_logo.png"))
        # A tip key is used as a shortcut to press the button
        # To show the TipKeys press and release the ALT key.
        # Over the "Developer" tab "D" will be shown. Pressing "D"
        # will switch to the tab and will show "LB" over "Large Button"
        # Pressing "B" then "L" will press the button
        # If a TipKey is not set then one will be generated based on the
        # button name.
        control.tip_key = "BL"
        # Set the tooltip for the button
        control.tooltip = "An example of a button which shows a 'Hello World!' dialog when pressed."

        # Create a small button called "Small Button".
        # When pressed the self._show_dialog method will be called.
        control = group.add_button("Small Button", self._show_dialog)
        control.tip_key = "BS"
        # Set the size of the button
        control.size = flare.UIRibbonControl.Size.Small

        # Create a button which is disabled
        control = group.add_button("Disabled Button", self._show_dialog)
        control.tip_key = "BD"
        control.size = flare.UIRibbonControl.Size.Small
        control.enabled = False

        # Create a button which is checkable
        action = QtWidgets.QAction(main_window.widget())
        action.setCheckable(True)
        action.toggled.connect(self._checked_button_pressed)
        control = group.add_action("Checkable Button", action)
        control.tip_key = "BC"
        control.size = flare.UIRibbonControl.Size.Small

        # Adds a split button, when the top half of the button is pressed
        # it runs the _show_dialog method. If the bottom half is pressed
        # then it shows a menu.
        menu = QtWidgets.QMenu()
        action = menu.addAction("One")
        action.triggered.connect(self._qaction_triggered)

        action = menu.addAction("Two")
        action.triggered.connect(self._qaction_triggered)

        action = menu.addAction("Three")
        action.triggered.connect(self._qaction_triggered)

        control = group.add_button("Split", self._show_dialog, menu)
        control.tip_key = "BS"

        # Adds a button which shows a menu when pressed.
        menu = QtWidgets.QMenu()
        action = menu.addAction("One")
        action.triggered.connect(self._qaction_triggered)

        action = menu.addAction("Two")
        action.triggered.connect(self._qaction_triggered)

        control = group.add_menu("Menu", menu)
        control.tip_key = "BM"

        # Create 3 radio buttons
        rb1 = QtWidgets.QRadioButton("One")
        rb1.setChecked(True)
        rb2 = QtWidgets.QRadioButton("Two")
        rb3 = QtWidgets.QRadioButton("Three")

        # Group the radio buttons so only one can be checked at a time
        rb_group = QtWidgets.QButtonGroup(main_window.widget())
        rb_group.addButton(rb1)
        rb_group.addButton(rb2)
        rb_group.addButton(rb3)
        # When a radio button is clicked the _radio_button_pressed method is called
        rb_group.buttonPressed.connect(self._radio_button_pressed)

        # Add the 3 radio buttons to the ribbon
        control = group.add_widget("Radio Button", rb1)
        control.tip_key = "B1"
        control = group.add_widget("Radio Button", rb2)
        control.tip_key = "B2"
        control = group.add_widget("Radio Button", rb3)
        control.tip_key = "B3"

        # Add a spin box widget to the ribbon
        # When the value is changed the _spin_box_value_changed method is called
        self._spin_box = QtWidgets.QSpinBox()
        self._spin_box.setRange(0, 100)
        self._spin_box.setValue(15)
        self._spin_box.valueChanged.connect(self._spin_box_value_changed)
        control = group.add_widget("Spin Box", self._spin_box)
        control.tip_key = "SB"

        # Add a slider widget to the ribbon
        # When the value is changed the _slider_value_changed method is called
        self._slider = QtWidgets.QSlider(QtCore.Qt.Horizontal)
        self._slider.setMinimum(self._spin_box.minimum())
        self._slider.setMaximum(self._spin_box.maximum())
        self._slider.setValue(self._spin_box.value())
        self._slider.valueChanged.connect(self._slider_value_changed)
        control = group.add_widget("Slider:", self._slider)
        control.tip_key = "SL"

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    @staticmethod
    def _plugin_file(file_name):
        """Return the path to the file `file_name` in this package."""
        return os.path.join(os.path.dirname(os.path.realpath(__file__)), file_name)

    @staticmethod
    def _show_dialog(text="Hello World!"):
        """Show the dialog."""
        main_window = flare.main_window()
        dialog = QtWidgets.QDialog(main_window.widget())
        dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        layout = QtWidgets.QVBoxLayout(dialog)
        layout.addWidget(QtWidgets.QLabel(text, dialog))
        dialog.show()

    @classmethod
    def _qaction_triggered(cls):
        """Show the dialog."""
        cls._show_dialog()

    @classmethod
    def _checked_button_pressed(cls, checked):
        """Show the dialog showing if the button is checked."""
        cls._show_dialog(f"Is checked: {checked}")

    def _spin_box_value_changed(self, value):
        """Set the slider value to match the spin box."""
        self._slider.setValue(value)

    def _slider_value_changed(self, value):
        """Set the spin box value to match the slider."""
        self._spin_box.setValue(value)

    @classmethod
    def _radio_button_pressed(cls, widget):
        """Show which radio button was pressed."""
        widget.setChecked(True)
        cls._show_dialog(f"Radio button pressed: {widget.text()}")
