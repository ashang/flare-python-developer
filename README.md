# Flare Python Developer

Welcome to the [Flare](https://www.cresset-group.com/flare/) Python Developer repository. This repository contains a collection of free Python scripts which demonstrate how to write Flare (TM) extensions. After installing these extensions a new ribbon tab titled "Developer" will be added to the Flare GUI.

If you are not a developer and are looking for Flare extensions, please visit the [flare-python-extensions repository](https://gitlab.com/cresset/flare-python-extensions). For scripts which allows Flare functionality to be accessed from the command line, please visit the [flare-python-pyflare repository](https://gitlab.com/cresset/flare-python-pyflare).

The developer example extensions are provided to you on an "AS-IS" basis, however if you need help with these extensions or have suggestions of your own please contact [Cresset support](https://www.cresset-group.com/about-us/contact-us/).

## Installing

See the [installation guide](https://gitlab.com/cresset/flare-python-extensions/blob/master/README.md).

## Extensions Descriptions


### Context Menus (contextmenus.py)
Demonstrates how to add items to context menus.


### Download 1oit (download1oit.py)
Demonstrates how to download a protein from a remote server.

**New Ribbon Controls:**

*Developer -> Download -> 1oit* - Download the protein '1oit' from the RCSB.


### Qt Designer Example (qtdesignerexample)
Demonstrates how to show a dialog created in Qt Designer.

In this example we load the file 'timerdialog.ui' which was created in Qt Designer and display it in a dialog.

**New Ribbon Controls:**

*Developer -> Developer -> Qt Designer Example* - Open the dialog created in Qt Designer.

### Rest Example (restexample.py)
Demonstrates how to use a rest server to add properties to Ligand table when ligands are added.

**Callbacks:**

*flare.callbacks.ligands_added* - When a ligand is added background threads are started which calculates a property. The background thread then ask the main thread to add a property to the ligand. This example by default generates a value on the background thread instead of using a REST server. To use a REST server modify the the _calculate_data() method.


### Ribbon (ribbon)
Demonstrates how to create a ribbon tab and group containing various buttons and widgets.

## Scripts Descriptions

### Run REST Service (runrestservice.py)
A REST service which returns the atom count of a SDF file.

The service runs on localhost:8080. When a get request is received with a data payload of

a SDF file a response is sent containing the atom count.

This script can be used to test the restexample.py extension.

Flask needs to be installed to run this script:

> pyflare -m pip install flask flask_restful



To run this script:

> pyflare runrestservice.py

