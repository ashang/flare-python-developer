# -*- coding: utf-8 -*-
# Copyright (C) 2020 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Demonstrates how to add items to context menus."""
from PySide2 import QtCore, QtWidgets

from cresset import flare


@flare.extension
class ContextMenusExtension:
    """Add menu items to various context menus."""

    def load(self):
        """Load the extension."""
        flare.callbacks.atom_context_menu.add(self._atom_menu)
        flare.callbacks.ligand_context_menu.add(self._ligand_menu)
        flare.callbacks.pose_context_menu.add(self._pose_menu)
        flare.callbacks.role_context_menu.add(self._role_menu)
        flare.callbacks.ligand_table_header_context_menu.add(self._ligand_table_header_menu)
        flare.callbacks.protein_residue_context_menu.add(self._protein_residue_menu)
        flare.callbacks.protein_sequence_context_menu.add(self._protein_sequence_menu)
        flare.callbacks.protein_context_menu.add(self._protein_menu)

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    def _atom_menu(self, menu, atoms):
        picked_atoms = flare.main_window().picked_atoms

        action = menu.addAction("Atom Python Menu")
        action.triggered.connect(
            lambda: self._show_dialog(
                f"{len(atoms)} current atoms.\n{len(picked_atoms)} picked atoms."
            )
        )

    def _ligand_menu(self, menu, ligand):
        action = menu.addAction("Ligand Python Menu")
        action.triggered.connect(lambda: self._show_dialog(f"Opened on '{ligand.title}'."))

    def _pose_menu(self, menu, pose):
        action = menu.addAction("Pose Python Menu")
        action.triggered.connect(lambda: self._show_dialog(f"Pose SMILES '{pose.smiles()}'."))

    def _role_menu(self, menu, role):
        action = menu.addAction("Role Python Menu")
        action.triggered.connect(lambda: self._show_dialog(f"Role '{role.name}'."))

    def _ligand_table_header_menu(self, menu, header):
        action = menu.addAction("Header Python Menu")
        action.triggered.connect(lambda: self._show_dialog(f"Header '{header}'."))

    def _protein_residue_menu(self, menu, residue):
        action = menu.addAction("Residue Python Menu")
        action.triggered.connect(lambda: self._show_dialog(f"Residue '{str(residue)}'."))

    def _protein_sequence_menu(self, menu, sequence):
        action = menu.addAction("Sequence Python Menu")
        action.triggered.connect(
            lambda: self._show_dialog(f"Sequence '{sequence.chain} {sequence.name}'.")
        )

    def _protein_menu(self, menu, protein):
        action = menu.addAction("Protein Python Menu")
        action.triggered.connect(lambda: self._show_dialog(f"Protein '{protein.title}'."))

    @staticmethod
    def _show_dialog(text):
        """Show the dialog with the text."""
        main_window = flare.main_window()
        dialog = QtWidgets.QDialog(main_window.widget())
        dialog.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        layout = QtWidgets.QVBoxLayout(dialog)
        layout.addWidget(QtWidgets.QLabel(text, dialog))
        dialog.show()
