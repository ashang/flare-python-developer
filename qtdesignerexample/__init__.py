# -*- coding: utf-8 -*-
# Copyright (C) 2020 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Demonstrates how to show a dialog created in Qt Designer.

In this example we load the file 'timerdialog.ui' which was created in Qt Designer
and display it in a dialog.

Ribbon Controls:
    Developer -> Developer -> Qt Designer Example
        Open the dialog created in Qt Designer.
"""
import os
import threading
import time
import datetime

from PySide2 import QtCore, QtWidgets, QtUiTools

from cresset import flare

UI_FILE_PATH = os.path.join(os.path.dirname(os.path.realpath(__file__)), "timerdialog.ui")


class TimerDialog(QtWidgets.QDialog):
    """Dialog which displays a form created in Qt Designer."""

    def __init__(self, parent=None):
        """Create the dialog."""
        super().__init__(parent)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)

        self._thread = None
        self._stopping = False
        self._start_time = datetime.datetime.now()

        # Load the Qt Designer form
        file = QtCore.QFile(UI_FILE_PATH)
        file.open(QtCore.QFile.ReadOnly)
        loader = QtUiTools.QUiLoader()
        self._widget = loader.load(file, parent)

        # Add the form to this dialog
        layout = QtWidgets.QVBoxLayout(self)
        layout.setContentsMargins(0, 0, 0, 0)
        layout.addWidget(self._widget)

        # Connect the signals from buttons in the .ui form to functions in this class
        self._widget.start_button.pressed.connect(self._on_start_stop)
        self._widget.reset_button.pressed.connect(self._on_reset)

    def __del__(self):
        """Stop the running background thread."""
        if self._thread:
            self._stopping = True
            self._thread.join()

    def _on_start_stop(self):
        """Start or stop the timer."""
        if self._thread:
            # Stop the timer
            self._stopping = True
            self._thread.join()
            self._thread = None
            self._widget.reset_button.setEnabled(True)
            self._widget.start_button.setText("Start")
        else:
            # Start the timer
            # A background thread is created which updates the label in the .ui form
            self._stopping = False
            self._thread = threading.Thread(target=self._run)
            self._thread.start()
            self._widget.reset_button.setEnabled(False)
            self._widget.start_button.setText("Stop")

    def _on_reset(self):
        """Reset the timer to 0."""
        self._start_time = datetime.datetime.now()
        self._widget.time_label.setText("0")

    def _run(self):
        """Periodically updates the timer."""
        while not self._stopping:
            time_diff = datetime.datetime.now() - self._start_time
            # All UI updates must take place on the main thread
            # So _on_update_label is called on the main thread
            # with the value to display
            flare.invoke_later(self._on_update_label, args=(time_diff,))
            time.sleep(0.01)

    def _on_update_label(self, time_diff):
        """Set the value in the .ui form label to time_diff."""
        self._widget.time_label.setText(str(time_diff))


@flare.extension
class QtDesignerExampleExtension:
    """Create a ribbon tab containing a button to show the timer dialog."""

    def load(self):
        """Add a buttons to the ribbon."""
        tab = flare.main_window().ribbon["Developer"]
        tab.tip_key = "D"
        ribbon = tab["Qt Designer Example"]

        control = ribbon.add_button("Timer", self._show_timer_dialog)
        control.tooltip = "Open the dialog which was created in Qt Designer."
        control.tip_key = "T"

    @staticmethod
    def _show_timer_dialog():
        """Show the timer dialog."""
        dialog = TimerDialog(flare.main_window().widget())
        dialog.show()
