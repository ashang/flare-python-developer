# -*- coding: utf-8 -*-
# Copyright (C) 2020 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Demonstrates how to download a protein from a remote server.

Ribbon Controls:
    Developer -> Download -> 1oit
        Download the protein '1oit' from the RCSB.
"""
import urllib.request

from cresset import flare


@flare.extension
class Download1oitExtension:
    """Add a button which downloads 1oit."""

    def load(self):
        """Load the extension."""
        tab = flare.main_window().ribbon["Developer"]
        tab.tip_key = "D"
        group = tab["Download"]
        control = group.add_button("1oit", self._download_1oit)
        control.tooltip = "Download the protein '1oit' from the RCSB."
        control.tip_key = "D"

        print(f"Loaded {self.__class__.__module__}.{self.__class__.__name__}")

    @staticmethod
    def _download_1oit():
        """Download 1oit and add it to the protein role."""
        url = "http://files.rcsb.org/view/1oit.pdb"
        response = urllib.request.urlopen(url)
        text = response.read().decode("utf-8")

        project = flare.main_window().project
        project.proteins.extend(flare.read_string(text, "pdb"))
