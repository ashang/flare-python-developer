#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2020 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""A REST service which returns the atom count of a SDF file.

The service runs on localhost:8080. When a get request is received with a data payload of
a SDF file a response is sent containing the atom count.

This script can be used to test the restexample.py extension.

Flask needs to be installed to run this script:
    > pyflare -m pip install flask flask_restful

To run this script:
    > pyflare runrestservice.py
"""
import flask
import flask_restful

from rdkit import Chem


class AtomCountResource(flask_restful.Resource):
    """Count the number of atoms in a SDF file."""

    def get(self):
        """Return a plain text response containing the atom count."""
        mol = Chem.MolFromMolBlock(flask.request.data)
        response = flask.make_response(str(mol.GetNumAtoms()))
        response.headers["content-type"] = "text/plain"
        return response


if __name__ == "__main__":
    app = flask.Flask(__name__)
    api = flask_restful.Api(app)
    api.add_resource(AtomCountResource, "/")
    app.run(debug=True, port=8080)
